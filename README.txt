A.nnotate Integration Module
=======================================

The A.nnotate Integration Module allows you to synchronzie Drupal nodes with
the A.nnotate tool (http://a.nnotate.com).

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration
 * Theming Notes


INTRODUCTION
------------

A.nnotate is a PHP web application developed by Textensor Ltd
(http://a.nnotate.com), which allows users to view PDFs and annotate them
directly in the browser.

This Module is a utility that embeds the functionality of A.nnotate into
Drupal 7 as seamlessly as possible.  Most A.nnotate installations are not
meant to be used as a stand-alone application, but instead embedded into another
site, such as Drupal site.  This module was designed with this purpose in mind;
to integrate A.nnnotate functionality as part of your Drupal site.

Any entity that contains a file attachment field can be integrated with
A.nnotate.  This module gives you control for which entities and bundles you
would like to enable this functionality. For such entities, this module adds
an "Annotate This" link, which opens the A.nnotate interface inside of Drupal.


INSTALLATION
------------

1.  This module assumes you have either a local A.nnotate installation on your
    own server, or that you have purchased a subscription to a hosted A.nnotate
    instance with API access.

2.  Install this module as you would any other Drupal Module
    (http://drupal.org/node/70151)


CONFIGURATION
-------------

1.  This module will not do anything until you have configured it.  Visit
    the configuration page (http://YOURSITE/admin/config/services/a_nnotate)
    to complete this step.

2.  Fill in the A.nnotate API Information on the configuration page.  The
    following fields are required:

    a. A.nnotate API User
       Typically, this is the email of the A.nnotate administrator.

    b. API Key
       This is located at the bottom of your 'account' page in A.nnotate.

    c. The URL of your A.nnotate API
       This will be the URL to your A.nnotate installation, with '/php/'
       appended.  E.g. http://example.com/a.nnotate/php/

    d. Attachment Field
       You must select at least one attachment field to enable for Annotation;
       although, you can select as many as you like.  If your Drupal content
       does not contain any entities with file attachment fields, you will
       not be able to use this module until you create such a field.

    e. Cleanup on Maintenance
       This option will enable this module to automatically synchronized
       documents with A.nnotate, which can improve performance when users
       browse your site.  It will also attempt to fix any broken links.

    f. Delete A.nnotate Document when Deleting Drupal Entity
       This will cause the module to delete A.nnotate documents when a
       corresponding entity is deleted in Drupal.  If unchecked, documents in
       A.nnotate will become orphaned upon deletion of entities in Drupal.

    g. Action to Perform when A.nnotate Document is Missing
       If this option is set to "Regenerate", the Module will automatically
       re-upload a document to A.nnotate that has gone missing.

    h. Action to Perform when Drupal document has been updated or replaced
       Careful!

    i. User Synchronization Mode
       If 'Single User' is selected, all annotations in A.nnotate will appear
       to come from a single Drupal user entity (this module will let you
       select who that is).

       If 'Multi User' is selected, annotations in A.nnotat will appear to come
       from whomever is signed into Drupal.

3.  This module also allows you to set permissions for who is allowed to see
    and access the A.nnotate tool.

4.  You can verify that the API settings are correct by visiting the
    'A.nnotate Integration Statistics' page in the Reports menu.

5.  You can test the functionality of the module by creating an entity with
    a file attachment field, ensuring that entity type is enabled on the
    configuration page, and looking for a

6.  The 'Annotate This' link is an extra field, meaning that you can manipulate
    its display just as you would any other field.


THEMING NOTES
-------------

When you click on an 'Annotate This' link, you will notice that the A.nnotate
interface appears inside of an iframe.  You can style this iframe however you
like in your CSS. It has a DOM ID of 'a_nnotate_iframe'.

You may also wish to provide a custom template page for pages that include
A.nnotate IFRAMES.  This can be accomplished by adding the following template
suggestion file in your template directory:

  page--a_nnotate.tpl.php

For more information, see: http://drupal.org/node/1089656


DEVELOPMENT NOTES
-----------------

* Add inline help
 - Admin page - more in-depth descriptions of settings and their implications
 - See: http://drupal.org/node/632280
