<?php

/**
 * @file
 * Drupal A.nnotate Admin Report Page
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

/**
 * A.nnotate Module report page
 *
 * @return string HTML code
 */
function a_nnotate_admin_report_page() {

  //Connection Status
  $connection_status = (a_nnotate_obj('client')->test_connection()) ?
    "<span class='succeed'>" . t('Good') . "</span>" : "<span class='fail'>" . t('Not Working') . "</span>";
  $connection_status .= ' ' . t("(Refresh this page to test again)");

  //Output Table
  $table_data = array();
  $table_data[] = array('Connection Status', $connection_status);
  $table_data[] = array('Number of files uploaded to A.nnotate', '##');
  $table_data[] = array('Number of entities with eligible File Fields', '##');
  $table_data[] = array('Number of entities with files uploaded to A.nnotate', '##');

  //Totals for different field instances
  foreach (a_nnotate_obj('entityactions')->enabled_fields as $entity_type => $bundles) {

    foreach ($bundles as $bundle => $field) {
      $desc = $entity_type . ' &raquo; ' . $bundle . ' &raquo;  ' . $field;
      $desc = "<span class='instance_info'>$desc</span>";
      $table_data[] = array($desc . ' fields uploaded to A.nnotate', '##');
      $table_data[] = array($desc . ' fields not yet uploaded to A.nnotate', '##');
    }
  }

  $table_info = array(
    'header'     => array(),
    'rows'       => $table_data,
    'attributes' => array(),
    'caption'    => '',
    'colgroups'  => array(),
    'sticky'     => '',
    'empty'      => t("No data!  Hm?")
  );

  $msg = t('This page displays information related to the A.nnotate Integration.');
  if (user_access('administer site configuration')) {
    $msg .= ' ' . l(t('Visit the configuration Page to change settings.'), 'admin/config/services/a_nnotate');
  }

  $msg = "<p>$msg</p>";

  return $msg . theme_table($table_info);
}

//------------------------------------------------------------------------

function _a_nnotate_get_report_stats() {

  $stats = new stdClass;

  //Number of files


  //Number of entities
}

/* EOF: a_nnotate_admin_report.inc */