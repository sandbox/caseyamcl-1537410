<?php

/**
 * @file
 * Drupal A.nnotate Admin Configuration Page
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

function a_nnotate_admin_config_page() {

  return drupal_get_form('a_nnotate_admin_config_form');
}

//------------------------------------------------------------------------

function a_nnotate_admin_config_form_submit($form, &$form_state) {

  //Prep URL
  if (drupal_substr($form_state['values']['api_url'], -1) != '/') {
    $form_state['values']['api_url'] .= '/';
  }

  //Save the data
  foreach (a_nnotate_obj('config')->get_setting_names() as $name) {
    if (isset($form_state['values'][$name])) {

      if (is_string($form_state['values'][$name])) {
        $form_state['values'][$name] = trim($form_state['values'][$name]);
      }

      a_nnotate_obj('config')->set_setting($name, $form_state['values'][$name]);
    }
  }

  //Set Message
  drupal_set_message(t('Updated the A.nnotate Integration Module Configuration!'));
}

//------------------------------------------------------------------------

function a_nnotate_admin_config_form_validate($form, &$form_state) {

  if ( ! isset($form_state['values']['attachment_fields'])) {
    form_set_error('attachment_fields', "You must specify at least one file field to synchronize!");
  }

  if ($form_state['values']['user_sync_mode'] == 'single_user' && $form_state['values']['user_sync_single_user'] == '') {
    form_set_error('user_sync_single_user', "You must specify an A.nnotate signature to use with single user mode!");
  }
}

//------------------------------------------------------------------------

function a_nnotate_admin_config_form($form, &$form_state) {

  //Get Config
  $config = a_nnotate_obj('config');

  if ( ! a_nnotate_obj('client')->test_connection()) {
    drupal_set_message(t('API Settings not Complete.  You will not be able to sync with A.nnotate until you configure these.'), 'warning');
  }

  $form['api_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('API Settings')
  );

  $form['api_settings']['api_url'] = array(
    '#type'          => 'textfield',
    '#title'         => 'API URL',
    '#description'   => 'The URL to your A.nnotate API with trailing slash (e.g. http://YOUR-SITE.COM/annotate/php/)',
    '#required'      => TRUE,
    '#default_value' => $config->api_url
  );

  $form['api_settings']['api_user'] = array(
    '#type'          => 'textfield',
    '#title'         => 'API User',
    '#description'   => 'The User Account you use to connect to the A.nnotate API.  Typically the original administrator\'s email (see documentation).',
    '#required'      => TRUE,
    '#default_value' => $config->api_user
  );

  $form['api_settings']['api_key'] = array(
    '#type'          => 'textfield',
    '#title'         => 'API Key',
    '#description'   => 'The API Key for your A.nnotate Installation.  You can find this on your A.nnotate account page at the very bottom.',
    '#required'      => TRUE,
    '#default_value' => $config->api_key
  );

  $form['api_settings']['annotate_owner_username'] = array(
    '#type'          => 'textfield',
    '#title'         => 'A.nnotate User',
    '#description'   => 'The A.nnotate User to upload documents as.  You must create this account in your A.nnotate installation before setting it here.',
    '#required'      => TRUE,
    '#default_value' => $config->annotate_owner_username
  );


  $form['api_settings']['annotate_owner_password'] = array(
    '#type'          => 'password',
    '#title'         => 'A.nnotate user Password',
    '#description'   => 'The A.nnotate User password to upload documents as.  Ensure this matches the configured account in your A.nnotate installation',
    '#required'      => TRUE,
    "#attributes"    => array('value' => $config->annotate_owner_password) //default password hack override
  );

  $form['attachment_fieldset'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Attachment Field')
  );

  $attachment_field_options = _a_nnotate_admin_config_page_get_attachment_field_opts();

  if (count($attachment_field_options) > 0) {

    $form['attachment_fieldset']['attachment_fields'] = array(
      '#type'    => 'checkboxes',
      '#title'   => t('Select a field'),
      '#options' => $attachment_field_options,
      '#default_value' => $config->attachment_fields
    );
  }
  else {

    $form['attachment_fieldset']['attachment_fields'] = array(
      '#markup' => 'You must create a file attachment field in order to enable A.nnotate synchronization!'
    );

  }

  $form['doc_sync_options'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Document Synchronization Options')
  );

  $form['doc_sync_options']['clean_on_maintenance'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Cleanup on Maintenance'),
    '#description'   => t('Enable cleanup of orphaned links and deleted documents during Drupal maintenance.  Recommended.'),
    '#default_value' => $config->clean_on_maintenance
  );

  $form['doc_sync_options']['delete_annotate_docs'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Delete A.nnotate document when Drupal entity deleted'),
    '#description'   => t('Enable deletion of A.nnotate documents when Drupal enities are deleted.  WARNING: This will delete all annotations as well!  Enable with caution.'),
    '#default_value' => $config->delete_annotate_docs
  );

  $missing_annotate_doc_options = array(
    '0' => 'Do nothing.  But do not show an A.nnotate link for the record.',
    '1' => 'Regenerate a new document in A.nnotate'
  );

  $form['doc_sync_options']['missing_annotate_doc'] = array(
    '#type'          => 'radios',
    '#title'         => t('Action to perform when A.nnotate document is missing'),
    '#options'       => $missing_annotate_doc_options,
    '#default_value' => $config->missing_annotate_doc
  );



  $updated_drupal_doc_options = array(
    '0' => 'Do nothing.  Use the old version of the document in in A.nnotate',
    '1' => 'Replace the document in A.nnotate.  Warning: Destructive! Annotations will be lost!',
    '2' => 'Add a new document in A.nnotate, and use that from now on.  Old document will remain in A.nnotate, but will not be linked to.'
  );

  $form['doc_sync_options']['updated_drupal_doc'] = array(
    '#type'          => 'radios',
    '#title'         => t('Action to perform when Drupal document has been updated or replaced'),
    '#options'       => $updated_drupal_doc_options,
    '#default_value' => $config->updated_drupal_doc
  );

  $form['user_sync_options'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('User Synchronization Options')

  );

  $user_sync_mode_options = array(
    'multi_user'  => 'Multi User - Users will interact with A.nnotate documents using their Drupal accounts.',
    'single_user' => 'Single User - All annotations made to documents in A.nnotate will appear as a single user.'
  );

  $form['user_sync_options']['user_sync_mode'] = array(
    '#type'          => 'radios',
    '#title'         => t('User Synchronization Mode'),
    '#options'       => $user_sync_mode_options,
    '#description'   => t('Which user mode to use.  See help for more information.  Note: Changing this value will not affect prior annotations'),
    '#default_value' => $config->user_sync_mode
  );

  $form['user_sync_options']['user_sync_single_user'] = array(
    '#title'         => t('Single User A.nnotate Signature'),
    '#type'          => 'textfield',
    '#description'   => t('Enter a signature that will be used for A.nnotations for all users (recommended to use an email address)'),
    '#default_value' => $config->user_sync_single_user,
    '#states' => array(
      'visible' => array('input[name=user_sync_mode]' => array('value' => 'single_user'))
    )
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration')
  );

  return $form;
}

//------------------------------------------------------------------------

function _a_nnotate_admin_config_page_get_attachment_field_opts() {

  $field_instances = array();

  foreach (field_info_fields() as $f) {

    if ($f['type'] == 'file') {

      foreach ($f['bundles'] as $entitytype => $bundles) {
        foreach ($bundles as $bundlename) {
          //Add an option to the list for this/bundle field combo
          $finfo = field_info_instance($entitytype, $f['field_name'], $bundlename);

          $desc  = "<strong>";
          $desc .= $finfo['entity_type'] . ' - ' . $finfo['bundle'] . ': ';
          $desc .= $finfo['label'];
          $desc .= "</strong>";
          if ($finfo['description']) {
            $desc .=  " - " . $finfo['description'];
          }

          $key = $finfo['entity_type'] . ':' . $finfo['bundle'] . ':' . $f['field_name'];
          $field_instances[$key] = $desc;
        }
      }

    }
  }

  return $field_instances;

}

/* EOF: a_nnotate_admin_config_page.inc */