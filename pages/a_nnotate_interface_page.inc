<?php

/**
 * @file
 * Drupal A.nnotate Interface Page
 *
 * Displays the A.nnotate interface for Entities
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

/**
 * A.nnotate Module interface page
 *
 * @param string $entity_type
 * @param int $entity_id
 * @return string HTML code
 */
function a_nnotate_interface_page($entity_type, $entity_id) {

  //Default is an error occured unless everything else worked correctly
  $page_code = "<p>Cannot load A.nnotate Interface</p>";

  try {

    //Get the entity
    $entity = entity_load($entity_type, array($entity_id));
    $entity = array_shift($entity);
    if ( ! $entity) {
      drupal_not_found();
    }

    //Check the attachment field
    if ( ! a_nnotate_obj('entityactions')->get_sync_field_for_entity($entity, $entity_type)) {
      drupal_not_found();
    }

    //Try syncing the record
    $sync_rec = _a_nnotate_sync_entity($entity_type, $entity_id);

    //Get the user for which to Annotate the document
    if (a_nnotate_obj('config')->user_sync_mode == 'single_user') {
      $user_email = a_nnotate_obj('config')->user_sync_single_user;
    }
    else {
      global $user;
      $user_email = $user->mail;
    }

    //Load the page (and the included iFrame)
    $page_code  = _a_nnotate_interface_page_get_pageheader_code(entity_uri($entity_type, $entity), entity_label($entity_type, $entity));
    $page_code .= _a_nnotate_interface_page_get_iframe_html($sync_rec->aid->c, $sync_rec->aid->d, $user_email);

  } catch (AnnotateApiException $e) {
    drupal_set_message(t('A.nnotate Server Error'), 'error');
  } catch (AnnotateActionsException $e) {
    drupal_set_message(t('A.nnotate Client Interface Error'), 'error');
  } catch (A_nnotateException $e) {
    drupal_set_message(check_plain(t($e->getMessage())), 'error');
  }

  //Return it!
  return $page_code;
}

// ---------------------------------------------------------------------------

/**
 * Get the pageheader code for the interface page
 *
 * @param array|boolean $uri    Returned value from entity_uri()
 * @param string|boolean $label Returned value from entity_label()
 * @return string               HTML code
 */
function _a_nnotate_interface_page_get_pageheader_code($uri, $label) {

  //No label?  Use generic label
  $label = drupal_ucfirst($label ?: t('Document'));

  //Set Title
  drupal_set_title(t('Annotate !label', array('!label' => $label)));

  $listitems = array();

  if ($uri) {
    $listitems[] = l(t('Return to !label', array('!label' => $label)), $uri['path']);
  }

  $html  = '';
  $html .= theme_item_list(array(
    'items' => $listitems,
    'title' => '',
    'type'  => 'ul',
    'attributes' => array()
  ));

  return "<div class='annotate_toolbar'>" . $html . "</div>";
}

// ---------------------------------------------------------------------------

/**
 * Get the Iframe HTML for the interface page
 *
 * @param string $c
 * @param string $d
 * @param string $user
 * @return string
 */
function _a_nnotate_interface_page_get_iframe_html($c, $d, $user) {

  $aac = a_nnotate_obj('actions')->get_anonymous_access_code($c, $d);

  $params = array(
    'c'        => $c,
    'd'        => $d,
    'nobanner' => 1,
    'asig'     => $user,
    'aac'      => $aac
  );

  $src = a_nnotate_obj('config')->api_url . 'pdfnotate.php?' . a_nnotate_obj('client')->url_serialize($params);
  return "<iframe id='a_nnotate_iframe' src='{$src}'></iframe>";
}


/* EOF: a_nnotate_interface_page.inc */