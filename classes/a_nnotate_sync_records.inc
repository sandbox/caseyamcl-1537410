<?php

/**
 * @file
 * Drupal A.nnotate Sync Records API
 *
 * Manages database CRUD for A.nnotate module sync records
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

/**
 * A.nnotate Sync Record Class
 *
 * Manages reading and writing A.nnotate Document tracking records in the Drupal database
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */
class A_nnotate_sync_records {

  // ---------------------------------------------------------------------------

  /**
   * Return a sync record object for a A.nnotate ID or FALSE if it doesn't exist
   *
   * @param string $c  The 'c' identifier in A.nnotate
   * @param string $d  The 'd' (date) identifier in A.nnotate
   * @return object|boolean
   */
  public function get_sync_record_from_annotate_id($c, $d) {
    return $this->get_sync_record((object) array('c' => $c, 'd' => $d), 'annotate_id');
  }

  // ---------------------------------------------------------------------------

  /**
   * Return a sync record object for a Entity ID/Type or FALSE if it doesn't exist
   *
   * @param int|object $id  The Entity ID
   * @param string The Entity Type
   * @return object|boolean
   */
  public function get_sync_record_from_entity_id($eid, $etype) {
    return $this->get_sync_record((object) array('eid' => $eid, 'etype' => $etype), 'entity_id');
  }

  // ---------------------------------------------------------------------------

  /**
   * Return a sync record object or FALSE if it doesn't exist
   *
   * @param int|object $id  The A.nnotate ID (object with $c and $d properties) or Entity ID (int)
   * @param string $type    encoded annotate_id or entity_id/type
   * @return object|boolean
   */
  private function get_sync_record($id, $type) {

    switch ($type) {

      case 'entity_id':
        $q = db_select('a_nnotate', 'ba')
          ->fields('ba')
          ->condition('eid', json_encode($id))
          ->execute();
      break;
      case 'annotate_id':
        $q = db_select('a_nnotate', 'ba')
          ->fields('ba')
          ->condition('aid', json_encode($id))
          ->execute();
      break;
      default:
        throw new InvalidArgumentException("Invalid \$type sent to get_sync_record");
    }

    //Return the row or FALSE if the row does not exist
    if ($q->rowCount() > 0 ) {
      $obj = $q->fetchObject();
      $obj->aid = json_decode($obj->aid);
      $obj->eid = json_decode($obj->eid);
      return $obj;
    }
    else {
      return FALSE;
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Write a sync record to the database
   *
   * If the $eid already exists, it will overwrite the sync record in the
   * database.
   *
   * @param int $eid
   * @param string $etype
   * @param string $c
   * @param string $d
   * @param string $md5
   * @return object|boolean  The record that was inserted/updated, or FALSE if not inserted/updated
   */
  public function put_sync_record($entity_id, $entity_type, $c, $d, $md5) {

    $aid = json_encode(array('c' => $c, 'd' => $d));
    $eid = json_encode(array('eid' => $entity_id, 'etype' => $entity_type));

    $fields = array(
      'aid'  => $aid,
      'eid'  => $eid,
      'last_check' => time(),
      'md5'  => $md5
    );

    if ($this->get_sync_record_from_entity_id($entity_id, $entity_type)) {
      $q = db_update('a_nnotate')->condition('eid', $eid)->fields($fields)->execute();
    }
    else {
      $q = db_insert('a_nnotate')->fields($fields)->execute();
    }

    return $this->get_sync_record_from_entity_id($entity_id, $entity_type);
  }

  // ---------------------------------------------------------------------------

  /**
   * Update check timestamp for sync record
   *
   * @param int $baid
   * @return boolean
   */
  public function update_check_timestamp($baid) {

    $fields = array(
      'last_check' => time()
    );

    return db_update('a_nnotate')->condition('baid', $baid)->fields($fields)->execute();
  }

  // ---------------------------------------------------------------------------

  /**
   * Get all Sync Records
   */
  public function get_sync_records($limit = NULL) {

    $q = db_select('a_nnotate', 'a')->fields('a');

    //Order by oldest timestamp first
    $q->orderBy('last_check', 'ASC');

    //If there is a limit...
    if ( ! is_null($limit)) {
      $q->range(0, $limit);
    }

    $results = $q->execute();

    $out = array();
    while ($row = $results->fetchObject()) {

      $row->eid = json_decode($row->eid);
      $row->aid = json_decode($row->aid);
      $out[$row->eid->etype . ':' . $row->eid->eid] = $row;
    }

    return $out;
  }

}

/* EOF: a_nnotate_sync_records.inc */