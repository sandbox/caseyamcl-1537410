<?php

/**
 * @file
 * Drupal A.nnotate Entity Actions Class
 *
 * Manages A.nnotate module interaction with entities
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

class A_nnotate_entity_actions {

  /**
   * @var array
   */
  private $enabled_fields = array();

  /**
   * @var array
   */
  private $allowed_exts;

  /**
   * @var A_nnotate_sync_records
   */
  private $syncrecs;

  /**
   * @var array
   */
  private $enabled_entity_list = FALSE;

  //------------------------------------------------------------------------

  /**
   * Constructor
   *
   * @param A_nnotate_sync_records
   * A.nnotate Sync Records Object
   *
   * @param array $enabled_fields
   * An array of encoded enabled field definitions from the A.nnotate module config class
   *
   * @param array $allowed_exts
   * An optional array of filetype extensions to allow for transport to A.nnotate
   * from the config class.  If empty, then allow all filetypes
   */
  public function __construct($syncrecs, $enabled_fields = array(), $allowed_exts = array()) {

    $this->syncrecs = $syncrecs;

    $this->enabled_fields = $this->interpret_enabled_fields($enabled_fields);
    $this->allowed_exts = $allowed_exts;
  }

  //------------------------------------------------------------------------

  /**
   * Magic Method for access to $this->enabled_fields
   */
  public function __get($val) {

    if ($val == 'enabled_fields') {
      return $this->enabled_fields;
    }
  }

  //------------------------------------------------------------------------

  /**
   * Get Sync Field for Entity
   *
   * @param object $entity
   * A Drupal entity object
   *
   * @param string $type
   * The type of the entity (because we can't derive that from the entity itself!)
   * See: http://drupal.org/node/1433662
   *
   * @return string|boolean
   * The fieldname if it exists, or FALSE if the entity does not have a
   * field that is enabled for sync with A.nnotate
   */
  public function get_sync_field_for_entity($entity) {

    //Get the bundle
    $etype  = $entity->entity_type;
    $bundle = $entity->entity_bundle;

    //If the entity has a field name assigned and that field actually exists
    if (isset($this->enabled_fields[$etype][$bundle])) {
      $fieldname = $this->enabled_fields[$etype][$bundle];
      if (isset($entity->$fieldname)) {
        return $fieldname;
      }
    }

    //If made it here...
    return FALSE;
  }

  //------------------------------------------------------------------------

  /**
   * Check if an entity is ready to be synchronized
   *
   * Checks if an entity contains a field that is configured
   * for synchronziation with A.nnotate, and that file contains a
   * valid file extensions
   *
   * @param object $entity
   * A Drupal entity object
   *
   * @return boolean
   * TRUE if the entity is ready to be sync'd, FALSE otherwise
   */
  public function check_entity_ready_for_sync($entity) {

    //Get the field (if no field, then FAIL)
    $fieldname = $this->get_sync_field_for_entity($entity);

    if ($fieldname && $entity->$fieldname) {

      $finfo = $entity->$fieldname;
      $finfo = $finfo['und'][0];

      //Now check if the field contains a value and is of a valid filetype
      $ext = pathinfo($finfo['filename'], PATHINFO_EXTENSION);
      if (in_array($ext, $this->allowed_exts)) {

        return TRUE;

      }
    }

    //If made it here...
    return FALSE;

  }

  //------------------------------------------------------------------------

  /**
   * Encode Enabled Fields
   *
   * Encodes enabled fields for storage in the configuration
   *
   * @param string $entity_type
   * The entity type (node, user, etc)
   *
   * @param string $bundle
   * The bundle (biblio, page, book, user, etc)
   *
   * @param string $fieldname
   * The specific fieldname
   *
   * @return string
   * All three parameters, encoded for storage
   */
  public function encode_enabled_field($entity_type, $bundle, $fieldname) {
    return implode(':', array($entity_type, $bundle, $fieldname));
  }

  //------------------------------------------------------------------------

  /**
   * Interpret Enabled Fields
   *
   * Decodes the enabled fields as they are encoded in the configuration
   *
   * @param array $enabled_fields
   * An array of encoded fields
   *
   * @return array
   * An array of decoded field/bundle/entities
   */
  private function interpret_enabled_fields($enabled_fields) {

    $efields = array();

    foreach ($enabled_fields as $fld) {
      list($entity_type, $bundle, $field_name) = explode(':', $fld, 3);
      $efields[$entity_type][$bundle] = $field_name;
    }

    return $efields;
  }

  //------------------------------------------------------------------------

  /**
   * Get a list of entity types and IDs for entities that are enabled
   *
   * Useful for the maintenance script
   *
   * @return array
   * An array of sub-arrays.  Each sub-array has 'type' and 'id' keys
   */
  public function get_enabled_entities_list() {

    $enabled_entities = array();

    //Outer foreach loops should run only a few times (number of fields enabled)
    foreach ($this->enabled_fields as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $field_name) {
        $q = new EntityFieldQuery();
        $q->entityCondition('entity_type', $entity_type)->entityCondition('bundle', $bundle);

        //Inner foreach loops may run a large number of times
        foreach($q->execute() as $etype => $ematches) {
          foreach($ematches as $id => $info) {
            $enabled_entities[] = array('id' => $id, 'type' => $etype);
          }
        }
      }
    }

    return $enabled_entities;
  }

  //------------------------------------------------------------------------

  /**
   * Get an entity to check during maintenance
   *
   * @TODO: This logic will really slow down the execution of the program
   * for large collections.  See if there is a way to improve the logic
   * to make it faster
   *
   * @return array|boolean
   * An array containing 'id' and 'type' keys, or FALSE if no more records
   * to check during this execution
   */
  function get_entity_to_check() {

    if ($this->enabled_entity_list === FALSE) {

      watchdog('a_nnotate', 'Started Building Entity List for Maintenance Run', array(), WATCHDOG_DEBUG);

      $this->enabled_entity_list = array();
      $syncrecords = $this->syncrecs->get_sync_records();

      //Add unsynced entities to the front of the list
      foreach($this->get_enabled_entities_list() as $ent) {

        $key = $ent['type'] . ':' . $ent['id'];
        if ( ! isset($syncrecords[$key])) {
          array_unshift($this->enabled_entity_list, $ent);
        }
      }

      //Add already synced entities to the back of the list
      foreach(array_keys($syncrecords) as $sr) {
        $arr = explode(':', $sr);
        array_push($this->enabled_entity_list, array('id' => $arr[1], 'type' => $arr[0]));
      }

      watchdog('a_nnotate', 'Finshed Building Entity List for Maintenance Run', array(), WATCHDOG_DEBUG);
    }

    //Return a single item, or nothing...
    return array_shift($this->enabled_entity_list) ?: FALSE;
  }
}

/* EOF: a_nnotate_entity_actions.inc */