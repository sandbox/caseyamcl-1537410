<?php

/**
 * @file
 * Drupal A.nnotate Client Class
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

/**
 * AnnotateApiException
 */
class AnnotateApiException extends A_nnotateException {

  public function __construct($message, $code = 0, $previous = NULL) {

    //Watchdog if it exists
    if (is_callable('watchdog')) {
      watchdog('a_nnotate', "A.nnotate API Error: %msg", array('%msg' => $message), WATCHDOG_ERROR);
    }

    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);
  }


}

// ---------------------------------------------------------------------------

/**
 * Annotate Client Class
 *
 * Class for sending/managing HTTP requests to the A.nnotate server.  This uses
 * drupal_http_request and CURL (the latter is used for file uploads only, since
 * drupal_http_request doesn't seem to work for multiplart encoded data)
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */
class A_nnotate_client {

  /**
   * @var string
   */
  private $api_url = NULL;

  /**
   * @var string
   */
  private $api_user = NULL;

  /**
   * @var string
   */
  private $api_key = NULL;

  /**
   * @ string|boolean  Flag for internal use
   */
  private $file_upload_path = FALSE;

  // ---------------------------------------------------------------------------

  /**
   * Constructor
   *
   * @param string $annotate_url
   * Annotate URL, usually ends in "/php/" (e.g. http://example.com/annotate/php/)
   *
   * @param string $api_user
   * API User - The Administrative Email of the Annotate installation
   *
   * @param string $api_key
   * API Key - Listed on the account page of the Annotate installation
   */
  public function __construct($annotate_url, $api_user, $api_key) {

    //Add trailing slash to the URL
    if (drupal_substr($annotate_url, -1) != '/')
      $annotate_url .= '/';

    $this->api_url = $annotate_url;
    $this->api_user = $api_user;
    $this->api_key = $api_key;
  }

  // ---------------------------------------------------------------------------

  /**
   * Perform a request to the A.nnotate server
   *
   * If POST paramaters are sent, this method will generate a POST request.
   * Otherwise, it will generate a GET request
   *
   * @param string $function
   * @param array $get_params     Query Parameters (key/value)
   * @param string $post_params   POST Parameters
   * @param string $annotate_user
   * @return string|array         Depending on function call (usually an array)
   * @throws AnnotateApiException
   */
  public function request($function, $get_params = array(), $post_params = array(), $annotate_user = NULL) {

    //Fix the function name
    if (drupal_substr($function, -4) != '.php') {
      $function .= '.php';
    }

    //Generate the signed request URL
    $signature = $this->generate_req_signature($function, $annotate_user);

    //Setup request options
    $http_options = array();

    $req = $this->api_url . $function . '?' . $signature;

    //Append GET parameters
    if (count($get_params) > 0) {
      $req = $this->append_query_params($req, $get_params);
    }

    //If POST with FILE to upload, do that, but use CURL since drupal_http_request no worky
    if ($this->file_upload_path) {
      $post_params['Filedata'] = '@' . $this->file_upload_path;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_URL, $req);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
      $resp = curl_exec($ch);
      $result = (object) array(
        'code' => (string) curl_getinfo($ch, CURLINFO_HTTP_CODE),
        'data' => $resp,
        'error' => curl_error($ch)
      );
      curl_close($ch);
      unset($resp, $info);
    }
    elseif (count($post_params) > 0) { //Elseif normal post requst
      $http_options['headers']['Content-type'] = 'application/x-www-form-urlencoded';
      $http_options['method'] = 'POST';
      $http_options['data'] = $this->url_serialize($post_params);
    }

    //Do It - if not done so already by CURL for file transfer
    if ( ! isset($result)) {
      $result = drupal_http_request($req, $http_options);
    }

    //Handle Errors
    if ($result->code{0} != 2) {
      $msg = (isset($result->status_message)) ? $result->status_message : $result->error;
      throw new AnnotateApiException("HTTP Error: " . $result->code . ' - ' . $msg);
    }
    elseif (drupal_strlen($result->data) > 4 && drupal_substr($result->data, 0, 3) == 'ERR') {
      throw new AnnotateApiException(trim(drupal_substr($result->data, 4)));
    }

    //Try to decode the JSON response
    $json_out = json_decode($result->data);

    if ($json_out) {
      return $json_out;
    }
    else {
      return $result->data;
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Upload a file to A.nnotate
   *
   * This calls the uploadDocument.php Annotate API call and sends it the filedata,
   * along with any additional form fields you wish to send
   *
   * @param string $filepath
   * @param array $post_params
   * @param string $annotate_user
   */
  public function upload($filepath, $post_params = array(), $annotate_user = NULL) {

    //Do it
    $this->file_upload_path = realpath($filepath);
    $to_return = $this->request('uploadDocument', array(), $post_params, $annotate_user);

    //Reset flag
    $this->file_upload_path = FALSE;

    //Return data
    list($null, $d, $c) = explode(' ', $to_return);
    return (object) array('d' => $d, 'c' => $c);
  }

  // ---------------------------------------------------------------------------

  /**
   * Test the connection to the A.nnotate Server
   *
   * Attempts to perform a simple API call to the A.nnotate server
   *
   * @param boolean $pass_exception  If TRUE, will return an exception with the error
   * @return boolean
   */
  public function test_connection($pass_exception = FALSE) {

    try {
      $result = $this->request('apiGetVersion');
    } catch (AnnotateApiException $e) {

      if ($pass_exception) {
        throw $e;
      }
      else {
        return FALSE;
      }
    }

    if (drupal_substr($result, 0, 2) == 'OK' && strpos($result, ' ')) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Generate API Request Signature
   *
   * Per the API documentation: http://a.nnotate.com/api-reference.html
   *
   * @param string $function
   * @param string $annotate_user
   * @return string
   */
  private function generate_req_signature($function, $annotate_user = NULL) {

    //Annotate user defaults to api_key (admin user email)
    if (is_null($annotate_user)) {
      $annotate_user = $this->api_user;
    }

    //Get time
    $req_time = time();

    //Build string
    $str_to_sign = "$function\n{$this->api_user}\n$req_time";
    if ($annotate_user)
      $str_to_sign = $str_to_sign . "\n$annotate_user";

    //Build Hash Signature
    $sig = $this->hex2b64(hash_hmac('sha1', $str_to_sign, $this->api_key));

    //Return the request
    $out_str  = "api-user=" . rawurlencode($this->api_user);
    $out_str .= "&api-requesttime=" . $req_time;
    $out_str .= "&api-auth=" . rawurlencode($sig);
    if ($annotate_user)
      $out_str .= "&api-annotateuser=" . rawurlencode($annotate_user);

    return $out_str;
  }

  // ---------------------------------------------------------------------------

  /**
   * Append an array of query parameters to a URL
   *
   * @param string $url  The original URL
   * @param array $params  An array of key/value pairs to convert
   * @return string
   */
  public function append_query_params($url, $params) {

    //If there is already a '?' in the URL, assume an existing query string
    $url .= (strpos($url, '?')) ? '&' : '?';

    return $url . $this->url_serialize($params);
  }

  // ---------------------------------------------------------------------------

  /**
   * URL Serialize an array of key/value pairs
   *
   * @param array $params
   * @return string
   */
  public function url_serialize($params) {

    foreach ($params as $k => &$v) {
      $v = $k . '=' . rawurlencode($v);
    }

    return implode('&', $params);
  }

  // ---------------------------------------------------------------------

  /**
   * Helper Function (hex2b64)
   *
   * @param string $str
   * @return string
   */
  private function hex2b64($str) {

    $raw = '';

    for ($i=0; $i < strlen($str); $i+=2) {
      $raw .= chr(hexdec(substr($str, $i, 2)));
    }
    return base64_encode($raw);
  }

}

/* EOF */