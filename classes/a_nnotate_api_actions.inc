<?php

/**
 * @file
 * Drupal A.nnotate API Actions
 *
 * Wrapper class for A_nnotate_client
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

/**
 * AnnotateApiException
 */
class AnnotateActionsException extends A_nnotateException {

  public function __construct($message, $code = 0, Exception $previous = NULL) {

    //Watchdog if it exists
    if (is_callable('watchdog')) {
      watchdog('a_nnotate', "A.nnotate Exception: %msg", array('%msg' => $message), WATCHDOG_ERROR);
    }

    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);
  }
}

// ---------------------------------------------------------------------------

/**
 * Drupal A_nnotate API Actions Class
 *
 * This is a wrapper class around the A_nnotate_client class, which makes
 * certain common actions (create user, upload document, etc.) trivially easy to run
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */
class A_nnotate_api_actions {

  /**
   * @var $client  Provide access to the A.nnotate client
   */
  private $client;

  /**
   * @var $syncrecs  Provide access to synchronization records
   */
  private $syncrecs;

  // ---------------------------------------------------------------------------

  /**
   * Constructor - Load Dependencies
   *
   * @param A_nnotate_client $client
   * A_nnotate_client instance
   *
   * @param A_nnotate_sync_records $syncrecs
   * A_nnotate_sync_records instance
   */
  public function __construct(A_nnotate_client $client, A_nnotate_sync_records $syncrecs) {

    $this->client = $client;
    $this->syncrecs = $syncrecs;
  }


  // ---------------------------------------------------------------------------

  /**
   * Verify that a given A.nnotate user exists for a given Drupal Account
   *
   * @param string $email
   * The Drupal user email
   *
   * @param boolean $autocreate
   * If TRUE, A.nnotate will create the account if it doesn't exist
   *
   * @return string|boolean
   * The A.nnotate User signature, or FALSE if it doesn't exist/wasn't created
   */
  public function verify_annotate_user($email, $autocreate = TRUE) {

    if (in_array($email, $this->list_annotate_users())) {
      return TRUE;
    }
    elseif ($autocreate) {
      return ($this->create_annotate_user($email)) ? TRUE : FLASE;
    }
    else {
      return FALSE;
    }

  }

  // ---------------------------------------------------------------------------

  /**
   * Create an A.nnnotate user
   *
   * @param string $email
   * Drupal user email
   *
   * @return string|boolean
   * The A.nnotate User Signature
   */
  public function create_annotate_user($email) {

    if (in_array($email, $this->list_annotate_users())) {
      throw new AnnotateActionsException("Cannot create new user with email $email.  That user already exists!");
    }

    $query_params = array('sig' => $email);
    $result = $this->client->request('createAccount', $query_params, array(), $email);
    return $result;
  }

  // ---------------------------------------------------------------------------

  /**
   * Login as User
   *
   * @param string $user_email
   * The user's email to login as
   *
   * @return boolean
   * TRUE if login succeeded, FALSE if failed
   */
  public function login_as_user($user_email) {

    try {
      $this->client->request('loginAs', array('remember' => '1'), array(), 'casey.mclaughlin@cci.fsu.edu');
      return TRUE;
    }
    catch (AnnotateApiException $e) {
      return FALSE;
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Upload a document to A.nnotate
   *
   * Also creates a sync record for the document
   *
   * @param object $entity
   * A Drupal Entity object
   *
   * @param string $file_field
   * The field inside the entity that contains the file info to be uploaded
   *
   * @param string $owner
   * The document owner (email)
   *
   * @return object
   * The sync record object from the db that was created when the file was uploaded
   */
  public function upload_document($entity, $file_field, $owner) {

    //Check for correct data
    if ( ! isset($entity->$file_field) OR ! $entity->$file_field) {
      throw new AnnotateActionsException("The entity does not contain a value in the attchment field: " . $file_field);
    }

    //Get the filepath
    $ff = $entity->$file_field;
    $filepath = drupal_realpath($ff['und'][0]['uri']);
    if ( ! $filepath OR ! is_readable($filepath)) {
      throw new AnnotateActionsException("Could not find the file at Drupal file URI: " . $entity->$file_field['und']['uri']);
    }

    //Do the upload
    $title = (isset($entity->title)) ? $entity->title : "Unknown Title"; //@TODO make this better
    $result = $this->client->upload($filepath, array('desc' => $title), $owner);

    //Record the sync record
    $result = $this->syncrecs->put_sync_record($entity->entity_primary_id, $entity->entity_type, $result->c, $result->d, md5($filepath));

    if ($result) {
      return $result;
    }
    else {
      throw new Exception("Could not update the sync record database!");
    }
  }

  // ---------------------------------------------------------------------------

  /**
   * Verify that a document exists in A.nnotate
   *
   * @param string $c
   * The A.nnotate 'c' identifier for the document
   *
   * @param string $d
   * The A.nnotate 'd' identifier for the document
   *
   * @return boolean
   * TRUE if the document exists, FALSE otherwise
   */
  public function verify_document($c, $d) {

    try {
      $result = $this->client->request('listNotes', array('c' => $c, 'd' => $d));
    } catch (AnnotateApiException $e) {

      if (strpos($e->getMessage(), 'no document found for') !== FALSE) {
        return FALSE;
      }
      else {
        throw $e;
      }

    }

    //If made it here without exception or return
    return TRUE;
  }

  // ---------------------------------------------------------------------------

  /**
   * Delete a document in A.nnotate
   *
   * @param string $c
   * The A.nnotate 'c' identifier for the document
   *
   * @param string $d
   * The A.nnotate 'd' identifier for the document
   *
   * @return boolean
   * TRUE or an exception
   */
  public function delete_document($c, $d) {
    $result = $this->client->request('deleteDocument', array('c' => $c, 'd' => $d));
    return TRUE;
  }

  // ---------------------------------------------------------------------------

  /**
   * Get the anonymous access code for a document
   *
   * @param string $c
   * The A.nnotate 'c' identifier for the document
   *
   * @param string $d
   * The A.nnotate 'd' identifier for the document
   *
   * @return string
   * The Anonymous Access Code String
   */
  public function get_anonymous_access_code($c, $d) {

    //Check if document actually exists
    if ( ! $this->syncrecs->get_sync_record_from_annotate_id($c, $d)) {
      throw new AnnotateActionsException("Could not find document with $c and $d identifiers in sync record database table");
    }

    //Perform the API call
    $result = $this->client->request('apiGetAnonAuthCode', array('c' => $c, 'd' => $d));
    list($null, $code) = explode(' ', $result, 2);
    return $code;
  }

  // ---------------------------------------------------------------------------

  /**
   * List users in A.nnotate
   *
   * @param boolean $flatten
   * If FALSE, will return an object with two arrays (members and annotators)
   *
   * @return array
   * An array of users
   */
  public function list_annotate_users($flatten = TRUE) {

    $users = $this->client->request('listUsers');

    if ($flatten) {
      $users = array_merge((array) $users->members, (array) $users->annotators);
    }

    return $users;
  }

}

/* EOF: a_nnotate_api_actions.inc */