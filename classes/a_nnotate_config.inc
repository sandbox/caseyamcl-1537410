<?php

/**
 * @file
 * Drupal A.nnotate Config API - For Cleaner Config Setting Management
 *
 * Manages configuration settings for the A.nnotate integration Drupal Module
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

/**
 * A.nnotate Drupal Config Class
 *
 * This class acts as a config settings API, since the built-in Drupal
 * variable_get() and variable_set() aren't enough to keep our stuff clean.
 */
class A_nnotate_config {

  /**
   * Static object (used mainly in install)
   * @var A_nnotate_config
   */
  private static $this_obj;

  /**
   * Define Settings
   * @var array
   */
  private static $settings = array(
    'api_user'                => NULL,
    'api_key'                 => NULL,
    'api_url'                 => NULL,
    'annotate_owner_username' => NULL,
    'annotate_owner_password' => NULL,

    'attachment_fields'       => array(),
    'allowed_filetypes'       => array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'odf', 'odt', 'png', 'jpg', 'jpeg', 'gif'),

    'clean_on_maintenance'    => TRUE,
    'delete_annotate_docs'    => FALSE,
    'missing_annotate_doc'    => 0, //1 = Recreate, 0 = Do nothing
    'updated_drupal_doc'      => 2, //1 = Replace, 2 = Add New, 0 = Use Old

    'user_sync_mode'          => 'multi_user', //single_user or multi_user are valid values
    'user_sync_single_user'   => NULL
  );

  /**
   * @var stdClass (object)
   */
  private $current_settings = NULL;

  //------------------------------------------------------------------------

  /**
   * Get configuration settings as a singleton
   *
   * @return A_nnotate_Config_Class
   */
  public static function init() {

    if ( ! self::$this_obj)
      self::$this_obj = new A_nnotate_config();

    return self::$this_obj;
  }

  //------------------------------------------------------------------------

  /**
   * Magic Method Interface to Config Options
   *
   * @param string $setting_name
   * @return mixed
   */
  public function __get($setting_name = NULL) {

    return $this->get_setting($setting_name);

  }

  //------------------------------------------------------------------------

  /**
   * Get a single setting
   *
   * @param string $setting_name
   * @return mixed
   */
  public function get_setting($setting_name) {
    return $this->get_settings($setting_name);
  }

  //------------------------------------------------------------------------

  /**
   * Get a single setting or all settings
   *
   * @param string $setting_name
   * @return mixed
   */
  public function get_settings($setting_name = NULL) {

    //Get the current settings or defaults
    if ( ! $this->current_settings) {
      $this->current_settings = variable_get('a_nnotate', $this->get_setting_defaults());
    }

    if ($setting_name) {
      return (isset($this->current_settings->$setting_name)) ?
      $this->current_settings->$setting_name : self::$settings[$setting_name];
    }
    else {
      return $this->current_settings;
    }
  }

  //------------------------------------------------------------------------

  /**
   * Set settings
   *
   * @param array|string $settings
   * If sent a string, specify a single setting name. If array, send key/value pairs
   *
   * @throws Exception
   *
   */
  public function set_settings($settings) {

    $settings = (array) $settings;

    //Check for invalid settings
    $invalid_settings = array_diff(array_keys($settings), $this->get_setting_names());
    if (count($invalid_settings) > 0)
      throw new Exception("Cannot set a_nnotate settings that do not exist: " . implode(', ', $invalid_settings));

    //Get current settings and update the ones that were sent
    $curr_settings = $this->get_settings();

    //Update the ones that were sent
    foreach ($curr_settings as $n => $v) {
      if (array_key_exists($n, $settings)) {
        $curr_settings->$n = $settings[$n];
        unset($settings[$n]);
      }
    }

    //Update any new settings that remain
    foreach ($settings as $n => $v) {
      $curr_settings->$n = $v;
    }

    //Update em
    variable_set('a_nnotate', (object) $curr_settings);
  }

  //------------------------------------------------------------------------

  /**
   * Set a single setting
   *
   * @param string $name
   * @param mixed $value
   */
  public function set_setting($name, $value) {
    $this->set_settings(array($name => $value));
  }

  //------------------------------------------------------------------------

  /**
   * Get setting names
   *
   * @return array
   */
  public function get_setting_names() {
    return array_keys(self::$settings);
  }

  //------------------------------------------------------------------------

  /**
   * Get settings and their defaults
   *
   * @return array
   */
  public function get_setting_defaults() {
    return (object) self::$settings;
  }

  //------------------------------------------------------------------------

  /**
   * Unset all settings (very destructive!)
   *
   * Should be used during uninstallation
   */
  public function uninstall_settings() {

    variable_del('a_nnotate');

  }

}

/* EOF: a_nnotate_config_class.inc */