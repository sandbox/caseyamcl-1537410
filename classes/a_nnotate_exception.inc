<?php

/**
 * @file
 * Drupal A.nnotate Entity Exception Class
 *
 * Adds Watchdog logic to Exception
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

class A_nnotateException extends Exception {

  public function __construct($message, $code = 0, Exception $previous = NULL) {

    //Watchdog if it exists
    if (is_callable('watchdog')) {
      watchdog('a_nnotate', "A.nnotate Module Exception: %msg", array('%msg' => $message), WATCHDOG_ERROR);
    }

    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);
  }
}

/* EOF: a_nnotate_exception.inc */