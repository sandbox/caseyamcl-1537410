<?php

/**
 * @file
 * Installer for A.nnotate Integration Module
 *
 * @author Casey McLaughlin
 */

//------------------------------------------------------------------------

require_once(dirname(__FILE__) . '/classes/a_nnotate_config.inc');

//------------------------------------------------------------------------

/**
 * Implements hook_install().
 */
function a_nnotate_install() {

  // install default values
  $dsettings = A_nnotate_config::init()->get_setting_defaults();
  A_nnotate_config::init()->set_settings($dsettings);
}

//------------------------------------------------------------------------

/**
 * Implements hook_uninstall().
 */
function a_nnotate_uninstall() {

  // remote default values
  $dsettings = A_nnotate_config::init()->uninstall_settings();
}

//------------------------------------------------------------------------

/**
 * Implements hook_requirements().
 */
function a_nnotate_requirements($phase) {

  //phase will be 'install', 'update', or 'runtime'
  $req = array();

  //Check for CURL regardless of phase
  $req['curl_installed'] = array(
    'title' => 'CURL PHP Libraries installed',
    'description' => 'A.nnotate Integration requires CURL PHP Libraries.  See: http://php.net/manual/en/book.curl.php',
    'severity' => (is_callable('curl_init')) ? REQUIREMENT_OK : REQUIREMENT_ERROR
  );

  return $req;
}

//------------------------------------------------------------------------

/**
 * Implements hook_schema().
 */
function a_nnotate_schema() {

  $schema = array();

  $schema['a_nnotate'] = array(
    'description' => 'Keeps track of synchronization between drupal entities and A.nnotate',
    'fields' => array(
      'baid' => array(
        'description' => 'Primary key',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'description' => 'Entity ID and Type (json encoded id and type values)',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE
      ),
      'aid' => array(
        'description' => 'A.nnotate ID (json encoded d & c values)',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE
      ),
      'md5' => array(
        'description' => 'MD5 hash of the file',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE
      ),
      'last_check' => array(
        'description' => 'Last check UNIX timestamp',
        'type' => 'int',
        'not null' => TRUE
      ),
    ),
    'primary key' => array('baid')
  );

  return $schema;
}

/* EOF: a_nnotate.module */