<?php

/**
 * @file
 * Drupal A.nnotate API Actions
 *
 * Help File - Main
 *
 * @author Casey McLaughlin
 * @subpackage Drupal 7 A.nnotate Integration Module
 */

//------------------------------------------------------------------------

?>
<h3>About</h3>

<p>
  The A.nnotate Module allows you to seamlessly integrate your <?php echo l(t('A.nnotate'), 'http://a.nnotate.com'); ?>
  installation with Drupal using the A.nnotate API.  <?php echo l(t('A.nnotate'), 'http://a.nnotate.com'); ?> is a tool
  that allows you to collaboratively annotate PDFs, Word Docs, and other documents online in a browser.
</p>

<h3>Uses</h3>

<dl>
  Viewing PDFs and Word Docs in the Browser
  <dd>
    The A.nnotate Module allows you to view PDF and other documents inside the browser,
    eliminating the need to download them as file attachments.  The module creates an extra
    field in selected entity types for displaying a view-in-browser link.
  <dd>
</dl>

<dl>
  Collaboratively annotating documents with other members of your Drupal site
  <dd>
    The A.nnotate Module seamlessly synchronizes your user account with A.nnotate so that
    any annotations made to documents are associated with your Drupal account.
  </dd>
</dl>

<h3>More Information</h3>

<ul>
  <li><?php echo l(t('View README'), drupal_get_path('module', 'a_nnotate') . DIRECTORY_SEPARATOR . 'README.txt'); ?></li>
  <li><?php echo l(t('View Online Documentation'), 'http://example.com'); ?></li>
</ul>